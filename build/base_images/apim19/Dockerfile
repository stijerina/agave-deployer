# APIM 1.9 base image
# Image: jstubbs/apim19_base

FROM jstubbs/template_compiler
MAINTAINER Joe Stubbs

# packages
RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:openjdk-r/ppa
RUN apt-get update
RUN apt-get install -y openjdk-7-jdk
RUN apt-get install -y unzip vim

# networking
ADD hosts.j2 /tmp/hosts

# logs
ADD logrotate.conf.j2 /etc/logrotate.conf

# APIM
add wso2am-1.9.0.zip /wso2am-1.9.0.zip
RUN unzip /wso2am-1.9.0.zip
RUN mkdir -p /wso2am-1.9.0/repository/deployment/server/userstores
COPY mysql-connector-java-5.1.26-bin.jar /wso2am-1.9.0/repository/components/lib/mysql-connector-java-5.1.26-bin.jar
COPY apim_initd.j2 /etc/init.d/apim
COPY catalina-server.xml /wso2am-1.9.0/repository/conf/tomcat/
COPY _auth_failure_handler_.xml /wso2am-1.9.0/repository/deployment/server/synapse-configs/default/sequences/_auth_failure_handler_.xml

# this custom truststore has the incommon root CA installed as well as other certs from third party API servers we wire up.
COPY client-truststore.jks.new /wso2am-1.9.0/repository/resources/security/client-truststore.jks

# patches
COPY WSO2-CARBON-PATCH-4.2.0-1411/wso2carbon-version.txt /wso2am-1.9.0/bin/wso2carbon-version.txt
COPY WSO2-CARBON-PATCH-4.2.0-1411/patch1411/ /wso2am-1.9.0/repository/components/patches/patch1411
COPY WSO2-CARBON-PATCH-4.2.0-1421/patch1421/ /wso2am-1.9.0/repository/components/patches/patch1421
COPY WSO2-CARBON-PATCH-4.2.0-1965/patch1965/ /wso2am-1.9.0/repository/components/patches/patch1965

# tacc customizations
COPY admin_password_grant-2.0.jar wso2am-1.9.0/repository/components/lib/admin_password_grant-2.0.jar
COPY admin_grant_roles /admin_grant_roles
COPY authenticationendpoint /wso2am-1.9.0/repository/deployment/server/webapps/authenticationendpoint
RUN rm -rf /wso2am-1.9.0/repository/deployment/server/webapps/authenticationendpoint.war


# APIM templates
COPY templates_list /templates
COPY templates/carbon19.xml.j2 /wso2am-1.9.0/repository/conf/carbon.xml
COPY templates/identity.xml.j2 /wso2am-1.9.0/repository/conf/identity.xml
COPY templates/api-manager19.xml.j2 /wso2am-1.9.0/repository/conf/api-manager.xml
COPY templates/user-mgt.xml.j2 /wso2am-1.9.0/repository/conf/user-mgt.xml
COPY templates/synapse.properties.j2 /wso2am-1.9.0/repository/conf/synapse.properties
COPY templates/master-datasources.xml.j2 /wso2am-1.9.0/repository/conf/datasources/master-datasources.xml
COPY templates/hosted-identity-userstore.xml.j2 /wso2am-1.9.0/repository/deployment/server/userstores/hosted_id_domain_name.xml
COPY templates/remote-userstore.xml.j2 /wso2am-1.9.0/repository/deployment/server/userstores/remote_domain_name.xml
COPY templates/store_site.json /wso2am-1.9.0/repository/deployment/server/jaggeryapps/store/site/conf/site.json
COPY templates/publisher_site.json /wso2am-1.9.0/repository/deployment/server/jaggeryapps/publisher/site/conf/site.json

# core apis
COPY templates/core_apis_19/ /wso2am-1.9.0/repository/deployment/server/synapse-configs/default/api/

# additional apis to be mounted at run time.
RUN mkdir /apis

# entry
COPY entry.sh /entry.sh

EXPOSE 9443 8243 6284
VOLUME ["/wso2am-1.9.0/repository/deployment/server"]
ENTRYPOINT ["sh", "/entry.sh"]

