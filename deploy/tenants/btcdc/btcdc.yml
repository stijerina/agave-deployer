#--------
# GENERAL
# -------
# Whether or not to deploy HA Proxy -- set to True or False
ha_deployment: False

# Whether to load the dn values of the persistence layer in the auth compose file using --extra-hosts
update_auth_dns: true


# ----
# APIM
# ----
tenant_id: btcdc
agave_env: staging
host: api.biotech.cdc.gov
tenant_admin_role: Internal/btcdc-services-admin

agave_profiles_url: profiles.btcdc.agave.tacc.utexas.edu/profiles
core_api_protocol: http

deploy_admin_password_grant: True
access_token_validity_time: 14400
deploy_custom_oauth_app: False
update_apim_core_dns: False
apim_increase_global_timeout: False


# -----
# MYSQL
# -----
mysql_host: db.biotech.cdc.gov
mysql_port: 3306

# ------------------
# IDENTITY & CLIENTS
# ------------------

# whether or not to deploy the agave_id service container.
use_hosted_id: true

# When true, the services will not make any updates.
agave_id_read_only: False

# unigue id for the "domain name" of the userstore in APIM
hosted_id_domain_name: btcdc

# domain of the ldap instance
ldap_domain: db.biotech.cdc.gov

# full URL or service discovery token for the hosted LDAP instance (including port)
ldap_name: ldap://db.biotech.cdc.gov:389

# account to bind to the LDAP db
auth_ldap_bind_dn: cn=admin,dc=agaveapi

# base search directory for user accounts
ldap_base_search_dn: dc=agaveapi

# Whether or not to check the JWT; When this is False, certain features will not be available such as the
# "me" lookup feature since these features rely on profile information in the JWT.
agave_id_check_jwt: True

# Actual header name that will show up in request.META; value depends on APIM configuration, in particular
# the tenant id specified in api-manager.xml
jwt_header: HTTP_X_JWT_ASSERTION_BTCDC

# Absolute path to the public key of the APIM instance; used for verifying the signature of the JWT.
agave_id_apim_pub_key: /home/apim/public_keys/apim_default.pub

# APIM Role required to make updates to the LDAP database
agave_id_user_admin_role: Internal/btcdc-user-account-manager

# Whether or not the USER_ADMIN_ROLE before allowing updates to the LDAP db (/users service)
agave_id_check_user_admin_role: True

# Set USE_CUSTOM_LDAP = True to use a database with a different schema than the traditional Agave ldap (e.g. TACC
# tenant). Some specific fields will still be required, for example the uid field as the primary key.
use_custom_ldap: False

# Base URL of this instance of the service. Used to populate the hyperlinks in the responses.
agave_id_app_base: https://api.biotech.cdc.gov

# DEBUG = True turns up logging and causes Django to generate excpetion pages with stack traces and
# additional information. Should be False in production.
# Updated -- 8/2015: Due to a bug in django, we currently set this to true so that the ALLOWED_HOSTS filtering is
# not activated.
agave_id_debug: True

# Beanstalk connection info
beanstalk_server: db.biotech.cdc.gov
beanstalk_port: 11300
beanstalk_tube: dev.staging
beanstalk_srv_code: 0001-001
tenant_uuid: 0001411570898814

# list of additional APIs to automatically subscribe clients to
# agave_clients_additional_apis:


# web app configuration
deploy_webapp: true
mail_server: 127.0.0.1
mail_server_port: 1025
email_base_url: http://api.biotech.cdc.gov

# -------------

# These settings are only used when deploying the account sign up web application:
# SMTP - used for the email loop account verification:
# mail_server:
# mail_server_port:
# email_base_url:



# -----
# HTTPD
# -----
# cert file - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
cert_file: apache.crt

# cert key file - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
cert_key_file: apache.key

# add when mounting in a CA cert (not used for self-signed certs) - should be a path relative to the httpd directory contained within the tenant directory for this tenant
# inside the tenants directory: e.g. deploy/tenants/dev_staging/httpd
# ssl_ca_cert_file:


# --------
# HA Proxy
# --------
# Required for ha_deployment: True

hap_auth_ip_1: 129.114.99.149
hap_http_auth_port_1: 4080
hap_https_auth_port_1: 40443

hap_auth_ip_2: 129.114.7.198
hap_http_auth_port_2: 4080
hap_https_auth_port_2: 40443

# ----
# CORE
# ----
update_core_dns: true
mysql_core_host: db.biotech.cdc.gov
mysql_core_port: 3301
mongo_domain: db.biotech.cdc.gov
